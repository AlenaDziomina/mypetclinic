package com.example.petclinic.mapper;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(uses = ContactDetailsMapper.class)
public interface OwnerMapper {

    Owner map(OwnerDto dto);

    OwnerDto mapToDto(Owner entity);
}
