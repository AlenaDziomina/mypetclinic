package com.example.petclinic.mapper;

import com.example.petclinic.dto.OwnerDetailsDto;
import com.example.petclinic.entity.Owner;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(uses = {ContactDetailsMapper.class, PetMapper.class})
public interface OwnerDetailsMapper {

    OwnerDetailsDto mapToDetails(Owner entity);
}
