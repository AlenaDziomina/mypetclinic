package com.example.petclinic.mapper;

import com.example.petclinic.dto.SystemOptionDto;
import com.example.system.entity.SystemOptionEntity;
import org.mapstruct.Mapper;

@Mapper
public interface SystemOptionMapper {

    SystemOptionDto mapToDto(SystemOptionEntity entity);
}
