package com.example.petclinic.mapper;

import com.example.petclinic.dto.OwnerPetDto;
import com.example.petclinic.dto.PetDto;
import com.example.petclinic.entity.Pet;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {OwnerMapper.class, VisitMapper.class, OwnerQualifier.class})
public interface PetMapper {

    @Mapping(target = "owner", source = "owner", qualifiedByName = "ownerQualifier")
    @Mapping(target = "id", ignore = true)
    void map(@MappingTarget Pet pet, PetDto dto);

    PetDto mapToDto(Pet entity);

    OwnerPetDto mapToOwnerDetailsPet(Pet pet);
}
