package com.example.petclinic.mapper;

import com.example.petclinic.dto.VetDto;
import com.example.petclinic.entity.Vet;
import org.mapstruct.Mapper;

@Mapper
public interface VetMapper {

    VetDto mapToDto(Vet vet);

    Vet map(VetDto dto);
}
