package com.example.petclinic.mapper;

import com.example.petclinic.dto.VisitDto;
import com.example.petclinic.entity.Visit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface VisitMapper {

    @Mapping(target = "description", source = "notes.description")
    VisitDto mapToDto(Visit entity);
}
