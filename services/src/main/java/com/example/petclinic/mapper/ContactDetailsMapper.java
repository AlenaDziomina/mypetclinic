package com.example.petclinic.mapper;

import com.example.petclinic.dto.ContactDetailsDto;
import com.example.petclinic.entity.ContactDetails;
import org.mapstruct.Mapper;

@Mapper
public interface ContactDetailsMapper {

    ContactDetailsDto mapToDto(ContactDetails details);

    ContactDetails map(ContactDetailsDto dto);
}
