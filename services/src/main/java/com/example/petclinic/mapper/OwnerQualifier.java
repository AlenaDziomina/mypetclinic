package com.example.petclinic.mapper;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.repository.OwnerRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class OwnerQualifier {

    private final OwnerRepository ownerRepository;

    @Named("ownerQualifier")
    public Owner getOwner(OwnerDto dto) {
        if (dto == null) {
            return null;
        }

        return ownerRepository.findById(dto.getId()).orElseThrow();
    }
}
