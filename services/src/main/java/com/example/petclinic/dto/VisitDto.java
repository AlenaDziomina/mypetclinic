package com.example.petclinic.dto;

import lombok.Data;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
public class VisitDto {

    private UUID id;
    private OffsetDateTime time;
    private String description;
}
