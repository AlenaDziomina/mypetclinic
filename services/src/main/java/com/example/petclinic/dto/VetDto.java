package com.example.petclinic.dto;

import lombok.Data;

@Data
public class VetDto {

    private Long id;
    private String specification;
    private String firstName;
    private String lastName;
    private String fullName;

}
