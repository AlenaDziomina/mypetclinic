package com.example.petclinic.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.OffsetDateTime;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Data
public class OwnerDto {

    private Long id;
    @JsonProperty("details")
    @Valid
    private ContactDetailsDto contactDetails;
    @NotBlank(message = "Owner name should not be blanc")
    private String firstName;
    @NotBlank
    private String lastName;
    private String fullName;

    private OffsetDateTime createdAt;
    private String createdBy;
    private OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;

    public boolean isNew() {
        return this.id == null;
    }
}
