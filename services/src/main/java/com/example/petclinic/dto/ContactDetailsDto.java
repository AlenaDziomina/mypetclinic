package com.example.petclinic.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ContactDetailsDto {

    @NotEmpty
    private String phone;
    private String city;
    private String street;
    private String building;
}
