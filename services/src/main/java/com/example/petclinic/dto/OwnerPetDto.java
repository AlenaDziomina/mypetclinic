package com.example.petclinic.dto;

import com.example.petclinic.entity.PetType;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class OwnerPetDto {

    private Long id;
    private String name;
    private LocalDate birthDate;
    private PetType petType;
    private List<VisitDto> visits;
}
