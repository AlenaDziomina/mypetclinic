package com.example.petclinic.dto;

import lombok.Data;

@Data
public class SystemOptionDto {

    private String id;
    private String value;
}
