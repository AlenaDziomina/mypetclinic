package com.example.petclinic.services;

import com.example.petclinic.dto.SystemOptionDto;

import java.util.List;

public interface SystemOptionService {

    List<SystemOptionDto> getAll();

    void save(SystemOptionDto option);

    SystemOptionDto getById(String optionKey);
}
