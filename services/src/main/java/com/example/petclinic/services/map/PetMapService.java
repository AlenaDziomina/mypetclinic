package com.example.petclinic.services.map;

import com.example.petclinic.dto.PetDto;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.MapImplementation;

import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class PetMapService extends AbstractMapService<PetDto, Long> implements PetService {

    private static final Map<Long, PetDto> resource = new HashMap<>();

    @Override
    public Map<Long, PetDto> getResource() {
        return resource;
    }

}
