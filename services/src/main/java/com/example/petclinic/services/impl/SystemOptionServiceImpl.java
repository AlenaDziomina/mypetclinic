package com.example.petclinic.services.impl;

import com.example.petclinic.dto.SystemOptionDto;
import com.example.petclinic.mapper.SystemOptionMapper;
import com.example.petclinic.services.SystemOptionService;
import com.example.system.entity.SystemOptionEntity;
import com.example.system.repository.SystemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SystemOptionServiceImpl implements SystemOptionService {

    private final SystemRepository systemRepository;
    private final SystemOptionMapper mapper;

    @Override
    public List<SystemOptionDto> getAll() {
        return systemRepository.findAll().stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public SystemOptionDto getById(String optionKey) {
        return systemRepository.findById(optionKey).map(mapper::mapToDto).orElseThrow();
    }

    @Transactional(transactionManager = "systemTransactionManager")
    @Override
    public void save(SystemOptionDto option) {
        SystemOptionEntity entity = systemRepository.findById(option.getId())
                .orElse(SystemOptionEntity.builder().id(option.getId()).build());

        entity.setValue(option.getValue());
        systemRepository.save(entity);
    }
}
