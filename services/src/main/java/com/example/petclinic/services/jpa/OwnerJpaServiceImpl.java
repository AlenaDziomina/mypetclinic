package com.example.petclinic.services.jpa;

import com.example.petclinic.dto.OwnerDetailsDto;
import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.exception.NotFoundException;
import com.example.petclinic.mapper.OwnerDetailsMapper;
import com.example.petclinic.mapper.OwnerMapper;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.config.JpaImplementation;
import com.example.petclinic.specification.SearchableRepository;
import com.example.petclinic.specification.SearchableService;
import com.example.petclinic.specification.filter.OwnerFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Transactional(readOnly = true)
@JpaImplementation
public class OwnerJpaServiceImpl extends AbstractJpaService<OwnerDto, Owner, Long> implements OwnerService, SearchableService<Owner> {

    @Autowired
    private OwnerRepository ownerRepository;
    @Autowired
    private PetRepository petRepository;
    @Autowired
    private OwnerMapper mapper;
    @Autowired
    private OwnerDetailsMapper detailsMapper;

    @Override
    public JpaRepository<Owner, Long> getRepository() {
        return ownerRepository;
    }

    @Override
    public SearchableRepository<Owner, ?> getSearchRepository() {
        return ownerRepository;
    }

    @Override
    public OwnerDto mapToDto(Owner entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Owner mapToEntity(OwnerDto dto) {
        return mapper.map(dto);
    }

    @Override
    public Collection<OwnerDto> findByName(String name) {
        return ownerRepository.findByLastNameLike(name).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    public void readUserAndPat() {
        List<Owner> owners = ownerRepository.findAll();
        for (Owner owner : owners) {
            log.warn("Owner name {}", owner.getFullName());
            for (Pet pet : owner.getPets()) {
                log.warn("Pet name: {}", pet.getName());
            }
        }
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void updateOwnerAndPet(Long id, String lastName, String petName) {
        Owner owner = ownerRepository.findById(id).orElseThrow();
        owner.setLastName(lastName);

        if (CollectionUtils.isNotEmpty(owner.getPets())) {
            log.info("Previous pet name: {}", owner.getPets().get(0).getName());

            updatePetName(owner.getPets().get(0).getId(), petName);
        }



//        ownerRepository.save(owner);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePetName(Long petId, String petName) {
        try {
            Pet pet = petRepository.findById(petId).orElseThrow();
            pet.setName(petName);
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }

    @Override
    public List<OwnerDto> findOwner(OwnerDto ownerDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Owner> example = Example.of(mapper.map(ownerDto), caseInsensitiveExMatcher);
        return ownerRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public Page<OwnerDto> search(OwnerFilter ownerFilter) {
        return searchPage(ownerFilter).map(mapper::mapToDto);
    }


    @Override
    public OwnerDetailsDto getOwnerDetails(Long id) {
        return ownerRepository.findById(id).map(detailsMapper::mapToDetails).orElseThrow(() -> new NotFoundException("Owner not exist"));
    }

    public void blockUnusedOwners() {
        ownerRepository.findAll().stream().forEach(user -> {
                    //todo
                }
        );
    }
}
