package com.example.petclinic.services;

import com.example.petclinic.dto.VetDto;
import com.example.petclinic.entity.Vet;

import java.util.Collection;

public interface VetService extends CrudService<VetDto, Long> {

    default Collection<Vet> findBySpec(String spec) {
        throw new UnsupportedOperationException();
    }
}
