package com.example.petclinic.services;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.dto.PetDto;

public interface PetService extends CrudService<PetDto, Long> {

    default PetDto finaByOwner(OwnerDto owner) {
        throw new UnsupportedOperationException();
    }

    default void updatePetName(Long petId, String petName) {
        throw new UnsupportedOperationException();
    }
}
