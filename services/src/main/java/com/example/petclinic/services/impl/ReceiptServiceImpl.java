package com.example.petclinic.services.impl;

import com.example.petclinic.aspect.ActivityLog;
import com.example.petclinic.entity.ReceiptStatus;
import com.example.petclinic.repository.ReceiptRepository;
import com.example.petclinic.services.ReceiptService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ReceiptServiceImpl implements ReceiptService {

    private final ReceiptRepository receiptRepository;

    @ActivityLog(activity = "receipt_cancel", value = "Receipt cancelled {id}")
    @Override
    public void cancelReceipt(Long id) {
        //todo validation
        receiptRepository.findById(id).orElseThrow().setStatus(ReceiptStatus.CANCELLED);
    }

    @ActivityLog(activity = "receipt_payed", value = "Receipt payed {id}")
    @Override
    public void markReceiptAsPayed(Long id) {
        //todo validation
        receiptRepository.findById(id).orElseThrow().setStatus(ReceiptStatus.PAYED);
    }
}
