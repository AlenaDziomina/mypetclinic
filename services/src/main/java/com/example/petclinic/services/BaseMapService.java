package com.example.petclinic.services;

import java.util.Map;

public interface BaseMapService<D, ID> {

    Map<ID, D> getResource();
}
