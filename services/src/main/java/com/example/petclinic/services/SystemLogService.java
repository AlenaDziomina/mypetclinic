package com.example.petclinic.services;

import com.example.petclinic.dto.SystemLogDto;

import java.util.List;

public interface SystemLogService {

    List<SystemLogDto> getAll();

    void createLogs(String activity, String message);
}
