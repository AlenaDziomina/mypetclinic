package com.example.petclinic.services.jpa;

import com.example.petclinic.dto.VetDto;
import com.example.petclinic.entity.Vet;
import com.example.petclinic.mapper.VetMapper;
import com.example.petclinic.repository.VetRepository;
import com.example.petclinic.services.VetService;
import com.example.petclinic.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

@RequiredArgsConstructor
@JpaImplementation
public class VetJpaServiceImpl extends AbstractJpaService<VetDto, Vet, Long> implements VetService {

    private final VetRepository vetRepository;
    private final VetMapper mapper;

    @Override
    public JpaRepository<Vet, Long> getRepository() {
        return vetRepository;
    }

    @Override
    public VetDto mapToDto(Vet entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Vet mapToEntity(VetDto dto) {
        return mapper.map(dto);
    }

    @Override
    public Collection<Vet> findBySpec(String spec) {
        return vetRepository.findBySpecification(spec);
    }
}
