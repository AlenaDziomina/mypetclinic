package com.example.petclinic.services.map;

import com.example.petclinic.dto.VetDto;
import com.example.petclinic.entity.Vet;
import com.example.petclinic.services.VetService;
import com.example.petclinic.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class VetMapService extends AbstractMapService<VetDto, Long> implements VetService {

    private static final Map<Long, VetDto> resource = new HashMap<>();

    @Override
    public Map<Long, VetDto> getResource() {
        return resource;
    }

}