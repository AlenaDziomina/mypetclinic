package com.example.petclinic.services;

import com.example.petclinic.dto.OwnerDetailsDto;
import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.specification.filter.OwnerFilter;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface OwnerService extends CrudService<OwnerDto, Long> {

    default Collection<OwnerDto> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    default List<OwnerDto> findOwner(OwnerDto ownerDto) {
        throw new UnsupportedOperationException();
    }

    default Page<OwnerDto> search(OwnerFilter ownerFilter) {
        throw new UnsupportedOperationException();
    }

    default OwnerDetailsDto getOwnerDetails(Long id) {
        throw new UnsupportedOperationException();
    }

    default void blockUnusedOwners() {
        //todo
    }
}
