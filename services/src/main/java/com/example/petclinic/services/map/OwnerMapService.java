package com.example.petclinic.services.map;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.config.MapImplementation;

import java.util.HashMap;
import java.util.Map;


@MapImplementation
public class OwnerMapService extends AbstractMapService<OwnerDto, Long> implements OwnerService {

    private static final Map<Long, OwnerDto> resource = new HashMap<>();

    @Override
    public Map<Long, OwnerDto> getResource() {
        return resource;
    }

}
