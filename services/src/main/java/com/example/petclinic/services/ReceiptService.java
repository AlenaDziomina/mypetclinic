package com.example.petclinic.services;

public interface ReceiptService {

    void cancelReceipt(Long id);

    void markReceiptAsPayed(Long id);
}
