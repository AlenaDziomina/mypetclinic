package com.example.petclinic.services.jpa;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.dto.PetDto;
import com.example.petclinic.entity.Cat;
import com.example.petclinic.entity.Dog;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.entity.PetType;
import com.example.petclinic.mapper.PetMapper;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class PetJpaServiceImpl extends AbstractJpaService<PetDto, Pet, Long> implements PetService {

    private final PetRepository petRepository;
    private final PetMapper mapper;

    @Override
    public JpaRepository<Pet, Long> getRepository() {
        return petRepository;
    }

    @Override
    public PetDto mapToDto(Pet entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Pet mapToEntity(PetDto dto) {
        Pet pet = dto.getId() == null ? getNewPet(dto) :
                petRepository.findById(dto.getId()).orElseThrow();
        mapper.map(pet, dto);
        return pet;
    }

    private Pet getNewPet(PetDto dto) {
        if (dto.getPetType() == PetType.CAT) {
            return new Cat();
        } else {
            return new Dog();
        }
    }

    @Override
    public PetDto finaByOwner(OwnerDto owner) {
        throw new UnsupportedOperationException();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePetName(Long petId, String petName) {
        Pet pet = petRepository.findById(petId).orElseThrow();
        pet.setName(petName);
    }
}
