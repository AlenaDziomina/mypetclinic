package com.example.petclinic.services.jpa;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.mapper.OwnerDetailsMapper;
import com.example.petclinic.mapper.OwnerMapper;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.PetRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OwnerJpaServiceImplTest {

    @InjectMocks
    OwnerJpaServiceImpl ownerJpaService;

    @Mock
    private OwnerRepository ownerRepository;
    @Mock
    private PetRepository petRepository;
    @Mock
    private OwnerMapper mapper;
    @Mock
    private OwnerDetailsMapper detailsMapper;

    @Test
    public void findByName() {
        //given
        String name = "test_name";
        Owner owner = Owner.builder().lastName(name).build();
        List<Owner> ownerList = List.of(owner);
        when(ownerRepository.findByLastNameLike(name)).thenReturn(ownerList);
        OwnerDto ownerDto = OwnerDto.builder().lastName(name).build();
        when(mapper.mapToDto(owner)).thenReturn(ownerDto);

        //when
        Collection<OwnerDto> result = ownerJpaService.findByName(name);

        //then
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(ownerDto, result.stream().findFirst().get());
    }
}