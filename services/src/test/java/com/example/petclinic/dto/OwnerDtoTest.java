package com.example.petclinic.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class OwnerDtoTest {

    OwnerDto ownerDto;

    @BeforeEach
    public void setUp() throws Exception {
        ownerDto = new OwnerDto();
    }

    @Test
    public void getId() {
        //given
        Long id = 123L;

        //when
        ownerDto.setId(id);

        //then
        assertEquals(id, ownerDto.getId());
    }

    @Test
    public void getFirstName() {
        //given
        String firstName = "first_name";

        //when
        ownerDto.setFirstName(firstName);

        //then
        assertEquals(firstName, ownerDto.getFirstName(), "first name not eq");
    }
}