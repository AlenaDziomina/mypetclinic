package com.example.petclinic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@PrimaryKeyJoinColumn(name = "owner_id")
@Entity
@Table(name = "VIP_CLIENT")
public class VipClient extends Owner {

    @Column(name = "VIP_CARD")
    private String vipCard;

    @Column(name = "DISCOUNT")
    private Long discount;

    @Column(name = "LOYALTY_PROGRAM")
    private String loyaltyProgram;

    @Column(name = "LOYALTY_AMOUNT")
    private Long loyaltyAmount;
}
