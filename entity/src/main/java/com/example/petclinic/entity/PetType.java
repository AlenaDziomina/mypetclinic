package com.example.petclinic.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public enum PetType {

    CAT(Values.CAT),
    DOG(Values.DOG);

    private static final Map<String, PetType> MAP = Arrays.stream(PetType.values())
            .collect(Collectors.toMap(PetType::getValue, Function.identity()));

    private final String value;

    public static PetType getByValue(String value) {
        if (Objects.isNull(value)) {
           return null;
        }
        return MAP.get(value);
    }

    public static class Values {
        public static final String CAT = "Cat1";
        public static final String DOG = "Dog2";
    }

}
