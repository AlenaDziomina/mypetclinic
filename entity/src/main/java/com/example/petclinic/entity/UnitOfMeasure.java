package com.example.petclinic.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "UOM")
public class UnitOfMeasure {

    @Id
    @Enumerated(EnumType.STRING)
    @Column(name = "CODE")
    private UOM code;

    @Column(name = "CODE_ISO")
    private String codeIso;

    @Column(name = "DESCRIPTION")
    private String description;

}
