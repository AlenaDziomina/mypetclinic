package com.example.petclinic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Table(name = "VET")
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Vet extends Person {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "SPECIFICATION")
    private String specification;

    @OneToMany(mappedBy = "vet")
    private List<Visit> visits;
}
