package com.example.petclinic.entity;

import com.example.petclinic.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

//@Setter no updates!
@Getter
@Entity
@View
@Table(name = "owner_receipt")
public class OwnerReceipt {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RECEIPT_ID")
    private Long receiptId;

    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @Column(name = "PAYER")
    private String payerName;

    @Column(name = "PAYER_PHONE")
    private String payerPhone;

}
