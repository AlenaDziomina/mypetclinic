package com.example.petclinic.entity;

import com.example.petclinic.converter.RoleConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "USER")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Convert(converter = RoleConverter.class)
    @Column(name = "ROLE")
    private Role role;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(name = "USER_PERM",
            joinColumns = @JoinColumn(name = "USR_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERM_ID")
    )
    private Set<Permission> permissions;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "USER_FILIALS",
            joinColumns = @JoinColumn(name = "USERS_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILIALS_ID")
    )
    private Set<Filial> filials;

    @Embedded
    private ContactDetails contactDetails;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "stree2")),
            @AttributeOverride(name = "building", column = @Column(name = "building2")),
            @AttributeOverride(name = "city", column = @Column(name = "city2")),
            @AttributeOverride(name = "phone", column = @Column(name = "phone2"))
    })
    private ContactDetails additional;

    @Builder.Default
    @Transient
    private boolean accountNonExpired = true;

    @Builder.Default
    @Transient
    private boolean accountNonLocked = true;

    @Builder.Default
    @Transient
    private boolean credentialsNonExpired = true;

    @Builder.Default
    @Transient
    private boolean enabled = true;

}
