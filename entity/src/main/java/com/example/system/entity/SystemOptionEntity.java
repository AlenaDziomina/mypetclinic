package com.example.system.entity;

import com.example.listener.SystemOptionEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(SystemOptionEntityListener.class)
@Entity
@Table(name = "SYSTEM_OPTION")
public class SystemOptionEntity {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "VALUE")
    private String value;
}
