package com.example.petclinic.repository;

import com.example.petclinic.entity.ContactDetails;
import com.example.petclinic.entity.Owner;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class OwnerRepositoryIT {

    @Autowired
    OwnerRepository ownerRepository;

    @Order(1)
    @Test
    public void test0_findAll() {
        List<Owner> result = ownerRepository.findAll();
        assertNotNull(result);
        assertEquals(0, result.size());
    }

    @Order(2)
    @Test
    public void test1_save() {
        //given
        Owner owner = Owner.builder()
                .firstName("fn")
                .lastName("ln")
                .contactDetails(ContactDetails.builder().phone("123").build())
                .build();

        //when
        Owner result = ownerRepository.save(owner);

        //then
        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCreatedAt());
        assertEquals("fn", result.getFirstName());
        assertEquals("ln", result.getLastName());
    }

    @Order(3)
    @Test
    public void test2_findAll() {
        List<Owner> result = ownerRepository.findAll();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Order(4)
    @Test
    public void test3_findByFirstName() {
        List<Owner> result = ownerRepository.findByFirstName("fn");
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Order(5)
    @Test
    public void test4_delete() {
        List<Owner> result = ownerRepository.findByFirstName("fn");
        Owner owner = result.get(0);

        ownerRepository.delete(owner);
    }

    @Order(6)
    @Test
    public void test5_findAll() {
        List<Owner> result = ownerRepository.findAll();
        assertNotNull(result);
        assertEquals(0, result.size());
    }
}