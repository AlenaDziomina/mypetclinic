package com.example.petclinic.controller;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.services.OwnerService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ActiveProfiles("test")
@WebMvcTest
class OwnerControllerIT extends BaseIT {

    @MockBean
    OwnerService ownerService;

    @DisplayName("Find owner")
    @Nested
    class FindOwner {

        @WithAnonymousUser
        @Test
        void findOwners_401() throws Exception {
            mockMvc.perform(get("/owners/find"))
                    .andExpect(status().isUnauthorized());
        }

        @WithMockUser("admin")
        @Test
        void findOwners() throws Exception {
            mockMvc.perform(get("/owners/find"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/findOwners"))
                    .andExpect(model().attributeExists("ownerFilter"));
        }

        @Test
        void findOwners_withpwd() throws Exception {
            mockMvc.perform(get("/owners/find").with(httpBasic("admin", "1111")))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/findOwners"))
                    .andExpect(model().attributeExists("ownerFilter"));
        }

        @Test
        void findOwners_user() throws Exception {
            mockMvc.perform(get("/owners/find").with(httpBasic("user", "2222")))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/findOwners"))
                    .andExpect(model().attributeExists("ownerFilter"));
        }

        @Test
        void findOwners_manager() throws Exception {
            mockMvc.perform(get("/owners/find").with(httpBasic("scott", "tiger")))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/findOwners"))
                    .andExpect(model().attributeExists("ownerFilter"));
        }
    }

    @DisplayName("Get owners (index)")
    @Nested
    class GetOwners {


        @Test
        void getOwners() throws Exception {
            mockMvc.perform(get("/owners/index"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/index"))
                    .andExpect(model().attributeExists("owners"));
        }

        @Test
        void getOwners_with() throws Exception {
            mockMvc.perform(get("/owners/index").with(anonymous()))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/index"))
                    .andExpect(model().attributeExists("owners"));
        }
    }

    @DisplayName("Init creation form")
    @Nested
    class InitCreationForm {

        @Test
        void initCreationForm() throws Exception {
            mockMvc.perform(get("/owners/new").with(anonymous()))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/createOrUpdateOwner"))
                    .andExpect(model().attributeExists("ownerDto"));
        }
    }

    @DisplayName("Process create owner")
    @Nested
    class ProcessCreate {

        @Test
        void processCreation_403() throws Exception {
            mockMvc.perform(post("/owners/new").with(anonymous()).with(csrf()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void processCreation_403user() throws Exception {
            mockMvc.perform(post("/owners/new").with(httpBasic("user", "2222")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void processCreation_403man() throws Exception {
            mockMvc.perform(post("/owners/new").with(httpBasic("scott", "tiger")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void processCreation_adm() throws Exception {
            mockMvc.perform(post("/owners/new").with(httpBasic("admin", "1111")).with(csrf()))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("InitUpdateForm")
    @Nested
    class InitUpdateForm {

        @Test
        void iniUpdateForm() throws Exception {
            when(ownerService.findById(1L)).thenReturn(OwnerDto.builder().build());
            mockMvc.perform(get("/owners/1/edit").with(anonymous()))
                    .andExpect(status().isOk())
                    .andExpect(view().name("owners/createOrUpdateOwner"))
                    .andExpect(model().attributeExists("ownerDto"));
        }
    }


    @DisplayName("Process update owner")
    @Nested
    class ProcessUpdate {

        @Test
        void processUpdate_403() throws Exception {
            mockMvc.perform(post("/owners/123/edit").with(anonymous()).with(csrf()))
                    .andExpect(status().isUnauthorized());
        }

        @ParameterizedTest(name = "#{index} with [{arguments}]" )
        @MethodSource("com.example.petclinic.controller.BaseIT#getUser")
        void processUpdate_403(String login, String password) throws Exception {
            mockMvc.perform(post("/owners/123/edit").with(httpBasic(login, password)))
                    .andExpect(status().isForbidden());
        }

        @ParameterizedTest(name = "#{index} with [{arguments}]" )
        @MethodSource("com.example.petclinic.controller.BaseIT#getNotUser")
        void processUpdate_ok(String login, String password) throws Exception {
            mockMvc.perform(post("/owners/123/edit")
                    .with(httpBasic(login, password)).with(csrf()))
                    .andExpect(status().isOk());
        }


    }
}