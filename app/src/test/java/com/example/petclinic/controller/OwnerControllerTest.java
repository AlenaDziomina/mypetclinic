package com.example.petclinic.controller;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.specification.filter.OwnerFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OwnerControllerTest {

    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private OwnerController ownerController;

    @Mock
    private Model model;


    @BeforeEach
    void setUp() {

    }

    @Test
    void getOwners() {
        //given

        Collection<OwnerDto> owners = mock(Collection.class);
        when(ownerService.findAll()).thenReturn(owners);
        //when
        String result = ownerController.getOwners(model);

        //then
        assertEquals("owners/index", result, " message test");
        verify(model, times(1)).addAttribute("owners", owners);
        verify(ownerService, times(1)).findAll();
    }

    @Test
    void processFindOwners() {
        //given
        OwnerFilter filter = mock(OwnerFilter.class);
        List<OwnerDto> ownerDtos = mock(List.class);
        Page<OwnerDto> page = mock(Page.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(ownerService.search(filter)).thenReturn(page);
        when(page.getContent()).thenReturn(ownerDtos);

        //when
        String result = ownerController.processFindOwners(filter, model, bindingResult);
        //then
        assertEquals("owners/ownersList", result);
        verify(model, times(1)).addAttribute("selections", ownerDtos);
        verify(ownerService, times(1)).search(filter);

    }

    @Test
    void processFindOwners_OneResult() {
        final long id = 3L;
        //given
        OwnerFilter filter = mock(OwnerFilter.class);
        List<OwnerDto> ownerDtos = mock(List.class);
        Page<OwnerDto> page = mock(Page.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(ownerService.search(filter)).thenReturn(page);
        when(page.getContent()).thenReturn(ownerDtos);
        when(ownerDtos.size()).thenReturn(1);
        OwnerDto ownerDto = mock(OwnerDto.class);
        when(ownerDtos.get(0)).thenReturn(ownerDto);
        when(ownerDto.getId()).thenReturn(id);
        //when
        String result = ownerController.processFindOwners(filter, model, bindingResult);
        //then
        assertEquals("redirect:/owners/3", result);
    }

    @Test
    void processFindOwners_EmptyResult() {
        //given
        OwnerFilter filter = mock(OwnerFilter.class);
        BindingResult bindingResult = mock(BindingResult.class);
        List<OwnerDto> ownerDtos = new ArrayList<>();
        Page<OwnerDto> page = mock(Page.class);
        when(ownerService.search(filter)).thenReturn(page);
        when(page.getContent()).thenReturn(ownerDtos);
        //when
        String result = ownerController.processFindOwners(filter, model, bindingResult);
        //then
        assertEquals("owners/findOwners", result);
        verify(bindingResult, times(1)).rejectValue("term", "notFound", "not found");
    }
}