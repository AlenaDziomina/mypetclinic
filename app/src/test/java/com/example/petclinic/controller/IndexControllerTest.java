package com.example.petclinic.controller;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.services.OwnerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IndexControllerTest {

    @InjectMocks
    IndexController indexController;

    @Mock
    OwnerService ownerService;


    @Test
    public void indexTest() {
        //given
        Model model = mock(Model.class);
        Collection<OwnerDto> ownerDtos = mock(Collection.class);
        when(ownerService.findAll()).thenReturn(ownerDtos);

        //when
        String result = indexController.indexTest(model);

        //then
        assertEquals("index_result", result);
        verify(model, times(1)).addAttribute(eq("test_attr"), eq("test_val"));
        verify(model, times(1)).addAttribute(eq("owners"), anyCollection());
    }
}