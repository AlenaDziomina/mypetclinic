package com.example.petclinic.controller;

import com.example.petclinic.entity.Role;
import com.example.petclinic.entity.UserEntity;
import com.example.petclinic.repository.CatRepository;
import com.example.petclinic.repository.DogRepository;
import com.example.petclinic.repository.ItemRepository;
import com.example.petclinic.repository.NotesRepository;
import com.example.petclinic.repository.OwnerReceiptRepository;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.PermissionRepository;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.repository.ProductRepository;
import com.example.petclinic.repository.ReceiptRepository;
import com.example.petclinic.repository.UOMRepository;
import com.example.petclinic.repository.UserRepository;
import com.example.petclinic.repository.VetRepository;
import com.example.petclinic.repository.VipClientRepository;
import com.example.petclinic.repository.VisitRepository;
import com.example.system.repository.SystemLogRepository;
import com.example.system.repository.SystemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

public abstract class BaseIT {

    @Autowired
    WebApplicationContext wac;

    protected MockMvc mockMvc;

    @MockBean
    CatRepository catRepository;
    @MockBean
    DogRepository dogRepository;
    @MockBean
    ItemRepository itemRepository;
    @MockBean
    NotesRepository notesRepository;
    @MockBean
    OwnerReceiptRepository ownerReceiptRepository;
    @MockBean
    OwnerRepository ownerRepository;
    @MockBean
    PetRepository petRepository;
    @MockBean
    PermissionRepository permissionRepository;
    @MockBean
    ProductRepository productRepository;
    @MockBean
    ReceiptRepository receiptRepository;
    @MockBean
    UOMRepository uomRepository;
    @MockBean
    UserRepository userRepository;
    @MockBean
    VetRepository vetRepository;
    @MockBean
    VipClientRepository vipClientRepository;
    @MockBean
    VisitRepository visitRepository;
    @MockBean
    SystemRepository systemRepository;
    @MockBean
    SystemLogRepository systemLogRepository;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
        UserEntity admin = UserEntity.builder().login("admin").password("{noop}1111").role(Role.ADMIN).build();
        UserEntity user = UserEntity.builder().login("user").password("{noop}2222").role(Role.USER).build();
        UserEntity scott = UserEntity.builder().login("scott").password("{noop}tiger").role(Role.MANAGER).build();

        when(userRepository.findByLogin("admin")).thenReturn(Optional.of(admin));
        when(userRepository.findByLogin("user")).thenReturn(Optional.of(user));
        when(userRepository.findByLogin("scott")).thenReturn(Optional.of(scott));
    }

    public static Stream<Arguments> getAllUsers() {
        return Stream.of(Arguments.of("admin", "1111"),
                Arguments.of("user", "2222"),
                Arguments.of("scott", "tiger")
        );
    }

    public static Stream<Arguments> getNotUser() {
        return Stream.of(Arguments.of("admin", "1111"),
                Arguments.of("scott", "tiger")
        );
    }

    public static Stream<Arguments> getUser() {
        return Stream.of(Arguments.of("user", "2222"));
    }

    public static Stream<Arguments> getNotAdmin() {
        return Stream.of(Arguments.of("user", "2222"),
                Arguments.of("scott", "tiger")
        );
    }
}
