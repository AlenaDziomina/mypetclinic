package com.example.petclinic.api;

import com.example.petclinic.controller.BaseIT;
import com.example.petclinic.services.OwnerService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest
class OwnerRestControllerIT extends BaseIT {

    @MockBean
    OwnerService ownerService;

    @Test
    void deleteOwner_401() throws Exception {
        mockMvc.perform(delete("/api/owners/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void deleteOwner() throws Exception {
        mockMvc.perform(delete("/api/owners/1")
                .header("Api-Key", "admin")
                .header("Api-Secret", "1111")
        )
                .andExpect(status().isOk());
    }

    @Test
    void deleteOwner_wrongPwd() throws Exception {
        mockMvc.perform(delete("/api/owners/1")
                .header("Api-Key", "admin")
                .header("Api-Secret", "wrong_pwd")
        )
                .andExpect(status().isUnauthorized());
    }

    @Test
    void deleteOwner_param() throws Exception {
        mockMvc.perform(delete("/api/owners/1")
                .param("apiKey", "admin")
                .param("apiSecret", "1111")
        )
                .andExpect(status().isOk());
    }

    @Test
    void deleteOwner_param_wrongPwd() throws Exception {
        mockMvc.perform(delete("/api/owners/1")
                .param("apiKey", "admin")
                .param("apiSecret", "wrong_pwd")
        )
                .andExpect(status().isUnauthorized());
    }

}