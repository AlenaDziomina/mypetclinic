INSERT INTO permission (name) values ('update_user');
INSERT INTO permission (name) values ('delete_user');
INSERT INTO permission (name) values ('create_visit');
INSERT INTO permission (name) values ('get_pet');

INSERT INTO uom (code, code_iso, description) values ('KG', 'KGR', 'kilo');
INSERT INTO uom (code, code_iso, description) values ('PACK', 'PACK', 'pack');
INSERT INTO uom (code, code_iso, description) values ('BOX', 'BOX', 'box big XL');