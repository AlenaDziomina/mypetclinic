package com.example.petclinic.api;

import com.example.petclinic.dto.PetDto;
import com.example.petclinic.services.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/pets")
@RestController
public class PetRestController {

    private final PetService petService;

    @GetMapping
    public Collection<PetDto> getAll() {
        return petService.findAll();
    }
}
