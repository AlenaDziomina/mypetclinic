package com.example.petclinic.api;

import com.example.petclinic.config.Runner;
import com.example.petclinic.entity.OwnerReceipt;
import com.example.petclinic.entity.UserEntity;
import com.example.petclinic.repository.OwnerReceiptRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class TestController {

    private final OwnerReceiptRepository ownerReceiptRepository;

    private final Runner runner;

    @GetMapping
    @Transactional
    public String test() {
        UserEntity user = runner.getUser();
        user.getPermissions().forEach(p -> System.out.println(p.getName()));
        return user.getLogin();
    }

    @GetMapping("/view")
    public List<OwnerReceipt> getView() {
        return ownerReceiptRepository.findByPayerNameContainsIgnoreCase("a");
    }


}
