package com.example.petclinic.api;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.security.RunAs;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.specification.filter.OwnerFilter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;


@RequiredArgsConstructor
@RequestMapping("/api/owners")
@RestController
public class OwnerRestController {

    private final OwnerService ownerService;

    @GetMapping
    public Collection<OwnerDto> getAll() {
        return ownerService.findAll();
    }

    @ApiOperation(value = "Search owners", response = Owner.class, responseContainer = "Page")
    @GetMapping("/search")
    public Page<OwnerDto> search(@RequestParam(required = false) String term,
                                 @RequestParam(required = false) Integer pageNumber) {

        OwnerFilter ownerFilter = OwnerFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return ownerService.search(ownerFilter);
    }

    @ApiOperation(value = "Search owners", response = Owner.class, responseContainer = "Page")
    @GetMapping("/search/by_name")
    public Page<OwnerDto> searchByName(@RequestParam(required = false) String name) {

        //todo
        return ownerService.search(null);
    }

    @ApiOperation(value = "Find owners", response = Owner.class, responseContainer = "List")
    @PutMapping("/find")
    public List<OwnerDto> findOwners(@ApiParam(value = "Search example") @RequestBody OwnerDto ownerDto) {
        return ownerService.findOwner(ownerDto);
    }

    @PostAuthorize("hasAuthority('ADNIN') || @tenancyAuthManager.checkPetOwner(authentication, returnObject.id)")
    @GetMapping("/{id}")
    public OwnerDto getById(@PathVariable("id") Long ownerId) {
        return ownerService.findById(ownerId);
    }

    @PostMapping
    public void create(@RequestBody OwnerDto ownerDto) {
        //todo impl
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long ownerId, @RequestBody OwnerDto ownerDto) {
        //todo impl
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long ownerId) {
        ownerService.delete(ownerId);
    }

    @PutMapping("/blockUsers")
    public void blockUsers() {
        RunAs.runAsSystem(ownerService::blockUnusedOwners);
    }
}
