package com.example.petclinic.api;

import com.example.petclinic.dto.VetDto;
import com.example.petclinic.services.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/vets")
@RestController
public class VetRestController {

    private final VetService vetService;

    @GetMapping
    public Collection<VetDto> getAll() {
        return vetService.findAll();
    }
}
