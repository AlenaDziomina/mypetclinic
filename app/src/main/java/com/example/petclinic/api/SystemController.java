package com.example.petclinic.api;

import com.example.petclinic.dto.SystemLogDto;
import com.example.petclinic.dto.SystemOptionDto;
import com.example.petclinic.services.SystemLogService;
import com.example.petclinic.services.SystemOptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api/system")
@RestController
public class SystemController {

    private final SystemLogService systemLogService;
    private final SystemOptionService systemOptionService;


    @GetMapping("/logs")
    public List<SystemLogDto> getLogs() {
        return systemLogService.getAll();
    }

    @GetMapping("/options")
    public List<SystemOptionDto> getSystem() {
        return systemOptionService.getAll();
    }

    @GetMapping("/options/{id}")
    public SystemOptionDto getById(@PathVariable("id") String optionKey) {
        return systemOptionService.getById(optionKey);
    }

    @PutMapping("/options")
    public void saveOption(@RequestBody SystemOptionDto option) {
        systemOptionService.save(option);
    }

}
