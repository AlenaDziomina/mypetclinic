package com.example.petclinic.controller;

import com.example.petclinic.dto.ContactDetailsDto;
import com.example.petclinic.dto.OwnerDetailsDto;
import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.specification.filter.OwnerFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/owners")
@Controller
public class OwnerController {

    private final OwnerService ownerService;

    @InitBinder
    public void setNotAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @GetMapping("/index")
    public String getOwners(Model model) {
        Collection<OwnerDto> owners = ownerService.findAll();
        model.addAttribute("owners", owners);
        return "owners/index";
    }

    @GetMapping("/{id}")
    public ModelAndView showOwner(@PathVariable(name = "id") Long ownerId) {
        ModelAndView modelAndView = new ModelAndView("owners/ownerDetails");
        OwnerDetailsDto ownerDto = ownerService.getOwnerDetails(ownerId);
        modelAndView.addObject("ownerDto", ownerDto);
        return modelAndView;
    }

    @GetMapping("/find")
    public String findOwners(Model model) {
        model.addAttribute("ownerFilter", OwnerFilter.builder().build());
        return "owners/findOwners";
    }

    @GetMapping
    public String processFindOwners(OwnerFilter ownerFilter, Model model, BindingResult bindingResult) {
        List<OwnerDto> result = ownerService.search(ownerFilter).getContent();
        if (CollectionUtils.isEmpty(result)) {
            bindingResult.rejectValue("term", "notFound", "not found");
            return "owners/findOwners";
        } else if (result.size() == 1) {
            OwnerDto ownerDto = result.get(0);
            return "redirect:/owners/" + ownerDto.getId();
        } else {
            model.addAttribute("selections", result);
            return "owners/ownersList";
        }
    }

    @GetMapping("/new")
    public String initCreationForm(Model model) {
        model.addAttribute("ownerDto", OwnerDto.builder()
                .contactDetails(ContactDetailsDto.builder().phone("+375-").build()).build());
        return "owners/createOrUpdateOwner";
    }


    @GetMapping("/{ownerId}/edit")
    public String iniUpdateForm(Model model, @PathVariable Long ownerId) {
        model.addAttribute("ownerDto", ownerService.findById(ownerId));
        return "owners/createOrUpdateOwner";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/new")
    public String processCreation(@Valid OwnerDto ownerDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "owners/createOrUpdateOwner";
        } else {
            ownerService.save(ownerDto);
            return "redirect:/owners/find";
        }
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    @PostMapping("/{ownerId}/edit")
    public String processUpdate(@Valid OwnerDto ownerDto, BindingResult bindingResult, @PathVariable Long ownerId) {
        if (bindingResult.hasErrors()) {
            return "owners/createOrUpdateOwner";
        } else {
            ownerDto.setId(ownerId);
            ownerService.save(ownerDto);
            return "redirect:/owners/" + ownerId;
        }
    }

}
