package com.example.petclinic.controller;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.services.OwnerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;

@RequiredArgsConstructor
@Controller
public class IndexController {

    private final OwnerService ownerService;

    @RequestMapping(value = {"", "/", "index", "index.html"})
    public String index() {
        System.out.println("test");
        return "index";
    }

    @RequestMapping(value = {"test"})
    public String indexTest(Model model) {
        System.out.println("test");
        Collection<OwnerDto> ownerDtos = ownerService.findAll();
        model.addAttribute("owners", ownerDtos);
        model.addAttribute("test_attr", "test_val");
        return "index_result";
    }

}
