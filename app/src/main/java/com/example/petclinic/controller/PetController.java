package com.example.petclinic.controller;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.dto.PetDto;
import com.example.petclinic.entity.PetType;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/owners/{ownerId}")
@Controller
public class PetController {

    private final PetService petService;
    private final OwnerService ownerService;

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @GetMapping("/pets")
    public String getPets(Model model) {
        model.addAttribute("pets", petService.findAll());
        return "pets/index";
    }

    @GetMapping("/pets/new")
    public String initCreationForm(OwnerDto ownerDto, Model model) {
        model.addAttribute("petDto", PetDto.builder().owner(ownerDto).petType(PetType.CAT).build());
        return "pets/createOrUpdatePet";
    }

    @GetMapping("/pets/{petId}/edit")
    public String initUpdateForm(@PathVariable Long petId, Model model) {
        model.addAttribute("petDto", petService.findById(petId));
        return "pets/createOrUpdatePet";
    }

    @PreAuthorize("hasAuthority('ADMIN') || @tenancyAuthManager.checkPetOwner(authentication, #ownerDto.id)")
    @PostMapping("pets/new")
    public String processCreate(OwnerDto ownerDto, @Valid PetDto petDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            model.put("petDto", petDto);
            return "pets/createOrUpdatePet";
        } else {
            petDto.setOwner(ownerDto);
            petService.save(petDto);

            return "redirect:/owners/" + ownerDto.getId();
        }
    }

    @PostMapping("/pets/{petId}/edit")
    public String processUpdate(@Valid PetDto petDto, @PathVariable Long petId, BindingResult bindingResult, OwnerDto ownerDto, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("petDto", petDto);
            return "pets/createOrUpdatePet";
        } else {
            petDto.setId(petId);
            petDto.setOwner(ownerDto);
            petService.save(petDto);
            return "redirect:/owners/" + ownerDto.getId();
        }
    }

    @ModelAttribute("types")
    public Collection<PetType> populatePetTypes() {
        return Arrays.asList(PetType.values());
    }

    @ModelAttribute("ownerDto")
    public OwnerDto findOwner(@PathVariable("ownerId") Long ownerId) {
        OwnerDto owner = ownerService.findById(ownerId);
        return owner;
    }

}
