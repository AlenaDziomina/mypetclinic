package com.example.petclinic.security;

import com.example.petclinic.entity.Role;
import com.example.petclinic.entity.UserEntity;
import com.example.petclinic.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;

@RequiredArgsConstructor
@Service
public class JpaUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByLogin(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));

        User user = new User(userEntity.getLogin(), userEntity.getPassword(), userEntity.isEnabled(),
                userEntity.isAccountNonExpired(), userEntity.isCredentialsNonExpired(),
                userEntity.isAccountNonLocked(), convertToAuthorities(userEntity.getRole()));

        return user;
    }

    private Collection<? extends GrantedAuthority> convertToAuthorities(Role role) {
        if (role != null) {
            return Collections.singleton(new SimpleGrantedAuthority(role.name()));
        } else {
            return Collections.emptySet();
        }
    }
}
