package com.example.petclinic.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;
import java.util.function.Consumer;

public class RunAs {

    public static <T> void runAsUser(String username, final T param, final Consumer<T> consumer) {
        final Authentication originAuth = SecurityContextHolder.getContext().getAuthentication();

        try {

            AnonymousAuthenticationToken token = new AnonymousAuthenticationToken(username, username,
                    Collections.singleton(new SimpleGrantedAuthority("ADMIN")));

            SecurityContextHolder.getContext().setAuthentication(token);
            consumer.accept(param);

        } finally {
            SecurityContextHolder.getContext().setAuthentication(originAuth);
        }
    }

    public static void runAsSystem(final Runnable runnable) {
        final Authentication originAuth = SecurityContextHolder.getContext().getAuthentication();

        try {

            AnonymousAuthenticationToken token = new AnonymousAuthenticationToken("system", "system",
                    Collections.singleton(new SimpleGrantedAuthority("ADMIN")));

            SecurityContextHolder.getContext().setAuthentication(token);
            runnable.run();

        } finally {
            SecurityContextHolder.getContext().setAuthentication(originAuth);
        }
    }
}
