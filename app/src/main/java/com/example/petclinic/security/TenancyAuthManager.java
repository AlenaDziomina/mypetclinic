package com.example.petclinic.security;

import com.example.petclinic.entity.UserEntity;
import com.example.petclinic.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TenancyAuthManager {

    private final UserRepository userRepository;

    public boolean checkPetOwner(Authentication authentication, Long id) {
        UserEntity userEntity = userRepository.findByLogin(authentication.getName()).orElseThrow();

        return userEntity.getId().equals(id);
    }
}
