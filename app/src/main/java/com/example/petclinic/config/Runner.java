package com.example.petclinic.config;

import com.example.petclinic.entity.Cat;
import com.example.petclinic.entity.ContactDetails;
import com.example.petclinic.entity.Dentist;
import com.example.petclinic.entity.Dog;
import com.example.petclinic.entity.Filial;
import com.example.petclinic.entity.Item;
import com.example.petclinic.entity.Notes;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Permission;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.entity.Product;
import com.example.petclinic.entity.ProductPK;
import com.example.petclinic.entity.Receipt;
import com.example.petclinic.entity.Role;
import com.example.petclinic.entity.Surgeon;
import com.example.petclinic.entity.UOM;
import com.example.petclinic.entity.UnitOfMeasure;
import com.example.petclinic.entity.UserEntity;
import com.example.petclinic.entity.VipClient;
import com.example.petclinic.entity.Visit;
import com.example.petclinic.repository.CatRepository;
import com.example.petclinic.repository.DogRepository;
import com.example.petclinic.repository.ItemRepository;
import com.example.petclinic.repository.NotesRepository;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.PermissionRepository;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.repository.ProductRepository;
import com.example.petclinic.repository.ReceiptRepository;
import com.example.petclinic.repository.UOMRepository;
import com.example.petclinic.repository.UserRepository;
import com.example.petclinic.repository.VetRepository;
import com.example.petclinic.repository.VipClientRepository;
import com.example.petclinic.repository.VisitRepository;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;


@RequiredArgsConstructor
@Component("mainRunner")
public class Runner implements CommandLineRunner {

    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;

    private final VisitRepository visitRepository;
    private final NotesRepository notesRepository;
    private final ProductRepository productRepository;
    private final ItemRepository itemRepository;
    private final ReceiptRepository receiptRepository;
    private final PermissionRepository permissionRepository;
    private final UserRepository userRepository;
    private final UOMRepository uomRepository;
    private final OwnerRepository ownerRepository;
    private final CatRepository catRepository;
    private final DogRepository dogRepository;
    private final PetRepository petRepository;
    private final VipClientRepository vipClientRepository;
    private final VetRepository vetRepository;

    @Override
    public void run(String... args) throws Exception {

//        createUser();

//        createPets();

//        createOwner();

//        createVisit();

//        createReceipt();

//        encription();
//
//
        //createProduct();
//


//        createVipClient();

//        createVet();

    }

    private void createVet() {
        Dentist dentist = Dentist.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .specification("dentist 1 categiry")
                .dentistDescription("dentist description")
                .build();

        Surgeon surgeon = Surgeon.builder()
                .firstName("Petr")
                .lastName("Petrov")
                .specification("surgeon 2 categiry")
                .surgeonDescription("surgeon description")
                .build();

        vetRepository.saveAll(Arrays.asList(surgeon, dentist));
    }

    private void createVipClient() {
        VipClient vipClient = VipClient.builder()
                .firstName("Alex")
                .lastName("Grouk")
//                .phone("+375-44-111-22-33")
                .vipCard("vip1234")
                .loyaltyAmount(120L)
                .loyaltyProgram("program_1")
                .discount(10L)
                .build();

        vipClientRepository.save(vipClient);
    }

    private void createPets() {
        Cat cat = Cat.builder()
                .birthDate(LocalDate.now())
                .catName("Barsic")
                .name("Barsik")
                .build();

        catRepository.save(cat);

        Dog dog = Dog.builder()
                .birthDate(LocalDate.now())
                .dogName("Reks")
                .name("Reks")
                .build();

        dogRepository.save(dog);

        Pet pet = Pet.builder()
                .birthDate(LocalDate.now())
                .name("undefined")
                .build();

        petRepository.save(pet);

        System.out.println("Pets in db " + petRepository.count());
    }

    private void createOwner() {
        List<Pet> pets = petRepository.findAll();

        Owner owner = Owner.builder()
                .firstName("Alena")
                .lastName("Dziomina")
                .pets(pets)
                .contactDetails(ContactDetails.builder()
                        .phone("+375-29-123-45-67")
                        .build())
                .build();

        for (Pet pet : pets) {
            pet.setOwner(owner);
        }

        ownerRepository.save(owner);
        petRepository.saveAll(pets);
    }

    @Transactional
    public UserEntity getUser() {
        UserEntity user = userRepository.findByLogin("user1").orElseThrow();
        System.out.println(user.getLogin());
        return user;
    }

    private void createUser() {

        Permission perm = permissionRepository.findByName("update_user")
                .orElseThrow(NoSuchElementException::new);

        Filial filial = Filial.builder()
                .filialName("filial1")
                .city("Minsk")
                .build();

        UserEntity user1 = UserEntity.builder()
                .login("user1")
                .role(Role.ADMIN)
                .password("1234")
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();

        UserEntity user2 = UserEntity.builder()
                .login("user2")
                .password("2222")
                .role(Role.MANAGER)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();
        userRepository.saveAll(Arrays.asList(user1, user2));
    }

    private void createReceipt() {
        List<Visit> visits = visitRepository.findAll();

        Receipt receipt1 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(0))
                .build();

        Item item1 = Item.builder()
                .name("coffee")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();
        Item item2 =  Item.builder()
                .name("pizza")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();

        receipt1.setItems(Arrays.asList(item1, item2));

        visits.get(0).setReceipt(receipt1);
        receiptRepository.save(receipt1);

        System.out.println("Amount in usd = " + receipt1.getAmountInUsd());

        Receipt receipt2 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(1))
                .build();
        visits.get(1).setReceipt(receipt2);
        receiptRepository.save(receipt2);
        visitRepository.saveAll(visits);
    }

    private void createProduct() {
        UnitOfMeasure uom = uomRepository.findById(UOM.PACK).orElseThrow();

        Product product1 = Product.builder()
                .name("Cola")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("001")
                        .codePart("abc")
                        .build())
                .unitOfMeasure(uom)
                .build();

        Product product2 = Product.builder()
                .name("Fanta")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("002")
                        .codePart("abc")
                        .build())
                .unitOfMeasure(uom)
                .build();

        productRepository.save(product1);
        productRepository.save(product2);
    }

    private void createVisit() {

        Pet pet = petRepository.findAll().get(0);

        Notes notes1 = Notes.builder().description("desc1").build();
        Notes notes2 = Notes.builder().description("desc2").build();

        Visit visit1 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes1)
                .pet(pet)
                .build();
        Visit visit2 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes2)
                .pet(pet)
                .build();
        visitRepository.saveAll(Arrays.asList(visit1, visit2));

    }


    private void encription() {
//        String pwd = "prod_pwd";
//        String encrypt = stringEncryptor.encrypt(pwd);
//        System.out.println(encrypt);
//
//        String decrypt = stringEncryptor.decrypt(encrypt);
//        System.out.println(decrypt);

        System.out.println(dbPwd);
    }
}
