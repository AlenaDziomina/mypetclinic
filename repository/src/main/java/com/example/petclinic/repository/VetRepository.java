package com.example.petclinic.repository;

import com.example.petclinic.entity.Vet;

import java.util.List;

public interface VetRepository extends PersonRepository<Vet, Long> {

    List<Vet> findBySpecification(String specification);
}
