package com.example.petclinic.repository;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.specification.SearchableRepository;

import java.util.List;

public interface OwnerRepository extends PersonRepository<Owner, Long>, SearchableRepository<Owner, Long> {

    List<Owner> findByCreatedByAndLastName(String createdBy, String lastName);

    List<Owner> findByLastNameOrFirstName(String lastName, String firstName);

    List<Owner> findByLastNameIgnoreCase(String lastName);

    List<Owner> findByLastNameOrFirstNameAllIgnoreCase(String lastName, String firstName);

    List<Owner> findByLastNameContaining(String name);

    List<Owner> findByLastNameLike(String name);

    List<Owner> findByLastNameAndContactDetailsPhone(String lastName, String phone);
}
