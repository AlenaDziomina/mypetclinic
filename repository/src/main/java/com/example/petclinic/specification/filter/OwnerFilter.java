package com.example.petclinic.specification.filter;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Owner_;
import com.example.petclinic.specification.OwnerSpecification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class OwnerFilter implements Filter<Owner> {

    private static final Sort DEFAULT_SORTING = Sort.by(Sort.Direction.DESC, Owner_.CREATED_AT);

    private Integer pageNumber;
    private String term;

    private String firstName;
    private String lastName;
    private boolean isActive;

    @Override
    public Pageable getPageable() {
        int page = Objects.isNull(pageNumber) ? 0 : pageNumber - 1;
        return PageRequest.of(page, DEFAULT_PAGE_SIZE, DEFAULT_SORTING);
    }

    @Override
    public Specification<Owner> getSpecification() {
        return OwnerSpecification.builder().filter(this).build();
    }
}
